SELECT		'Affirmed Network' AS level_1st,
                'UE A' AS level_2nd,
		NULL AS level_3rd,
		'Connect' AS connection_1st,
		'Connect' AS connection_2nd,
		NULL AS connection_3rd
UNION ALL
SELECT		'Affirmed Network' AS level_1st,
		'UE B' AS level_2nd,
		'Fci 001' AS level_3rd,
		'Connect' AS connection_1st,
		'Connect' AS connection_2nd,
		'Connect' AS connection_3rd
UNION ALL
SELECT		'Affirmed Network' AS level_1st,
		'UE B' AS level_2nd,
		'Fci 002' AS level_3rd,
		'Connect' AS connection_1st,
		'Connect' AS connection_2nd,
		'Connect' AS connection_3rd
UNION ALL
SELECT		'Affirmed Network' AS level_1st,
		'UE C' AS level_2nd,
		'Fci 003' AS level_3rd,
		'Connect' AS connection_1st,
		'Disconnect' AS connection_2nd,
		'Disconnect' AS connection_3rd
UNION ALL
SELECT		'Affirmed Network' AS level_1st,
		'UE D' AS level_2nd,
		NULL AS level_3rd,
		'Connect' AS connection_1st,
		'Disconnect' AS connection_2nd,
		NULL AS connection_3rd